﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Component_I
{

    /// <summary>
    /// Class circle for creating circle
    /// </summary>
    public class Circle: Form1, Shape
    {
       
        /// <summary>
        /// 
        /// </summary>
        public int s, n, j;  //defining variables of circle


        /// <summary>
        /// values of a circle
        /// </summary>
        /// <param name="k"></param>
        /// <param name="h"></param>
        /// <param name="u"></param>
        public void saved_values(int k, int h, int u) {
            n = k;
            j = h;
            s = u;
            
        }


        /// <summary>
        /// Method for drawing circle
        /// </summary>
        /// <param name="g"></param>
        public void Draw_shape(Graphics g)
        {
            Pen mew2 = Form1.defaultpen;
            SolidBrush me = Form1.sb;
            if (Form1.fill)
            {
                g.FillEllipse(me, n, j, s, s);     //gives filled ellipse 
            }
            else
            {
                g.DrawEllipse(mew2, n, j, s, s);     //gives not filled ellipse
            }
            
           
         

           


                }
    }
}
