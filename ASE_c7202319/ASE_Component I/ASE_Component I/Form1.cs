﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;

namespace ASE_Component_I
{
    /// <summary>
    /// Form1 designed for gui components.
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// X- axis of the shape to be produced.
        /// </summary>
        public int positionXaxis = 0;
        /// <summary>
        ///Y- axis of the shape to be produced.
        /// </summary>
        public int positionYaxis = 0;
        
       
        /// <summary>
        /// For graphics to be accessed in creating shapes.
        /// </summary>
        Graphics g;
        
        /// <summary>
        /// Text value in the textbox.
        /// </summary>
        string command;
        /// <summary>
        /// For multiple shapes to be produced, command is read on multiple lines..
        /// </summary>
        string[] multiLine;
        /// <summary>
        /// For multilines to be trimmed.
        /// </summary>
        String abc;
        /// <summary>
        /// For trimmed multilines to be splitted.
        /// </summary>
        public string[] syntax;
        /// <summary>
        /// If user chooses shapes not to be filled.
        /// </summary>
        public static bool fill = false;
        /// <summary>
        /// Default color of pen.
        /// </summary>
        public static Pen defaultpen = new Pen( Color.Black);
        /// <summary>
        /// Default color of solidbrush.
        /// </summary>
        public static SolidBrush sb = new SolidBrush(Color.Black);



        /// <summary>
        /// Method made for switching different pens.
        /// </summary>
        /// <param name="color"></param>
        public void penswitcher(string color)
        {
            if (!fill)
            {
                switch (color)                     //color assigned in switch
                {
                    case "blue":
                        {
                            defaultpen = new Pen(Color.Blue);
                            break;
                        }

                    case "green":
                        {
                            defaultpen = new Pen(Color.Green);
                            break;
                        }

                    case "yellow":
                        {
                            defaultpen = new Pen(Color.Yellow);
                            break;
                        }
                    case "red":
                        {
                            defaultpen = new Pen(Color.Red);
                            break;
                        }

                }
                MessageBox.Show("Pen color changed to "+ color);   //after changing pen color.

            }

            else
            {

                switch (color)
                {
                    case "blue":
                        {
                            sb = new SolidBrush(Color.Blue);
                            break;
                        }

                    case "green":
                        {
                            sb = new SolidBrush(Color.Green);
                            break;
                        }

                    case "yellow":
                        {
                            sb = new SolidBrush(Color.Yellow);
                            break;
                        }

                    case "red":
                        {
                            sb = new SolidBrush(Color.Red);
                            break;
                        }

                }
              

            }
        }

        /// <summary>
        /// Constructor of this class.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
           
        }
        /// <summary>
        ///  This is method to move pen.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void pentomove(int x, int y)
        {
            
            positionXaxis = x;
            positionYaxis = y;




        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// This is method for drawing in pen.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public void pentodraw(int a, int b)
        {
            
             g = panel1.CreateGraphics();
            g.DrawLine(defaultpen, positionXaxis, positionYaxis, a, b);
            positionXaxis = a;
            positionYaxis = b;
        }








       
       
        /// <summary>
        /// Clear button
        /// </summary>
        private void clear()
        {
            panel1.Refresh();
        }
        /// <summary>
        /// Resets the coordinates to top left position.
        /// </summary>
        private void reset()
        {
            positionXaxis = 0;
            positionYaxis = 0;
        }
        /// <summary>
        /// Defining sides of rectangle
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void rectangle_draw(int a, int b,int c, int d )
        {
            Rectangle snj1 = new Rectangle();
            snj1.saved_values(a, b, c, d);
            Graphics g = panel1.CreateGraphics();
            snj1.Draw_shape(g);
        }

        /// <summary>
        /// Defining the sides of square.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        public void square_draw(int a, int b, int c, int d)
        {
            Rectangle snj1 = new Rectangle();
            snj1.saved_values(a, b, c, d);
            Graphics g = panel1.CreateGraphics();
            snj1.Draw_shape(g);
        }


        /// <summary>
        /// Defining points of circle.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        public void circle_draw(int a, int b, int c)
        {
            Circle snj2 = new Circle();
            snj2.saved_values(a, b, c);
            Graphics g = panel1.CreateGraphics();
            snj2.Draw_shape(g);
        }

        /// <summary>
        /// Method to draw triangles shape.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        public void triangle_draw(int x, int y, int z, int w)
        {
            Triangle tri = new Triangle();
            tri.saved_values(x, y, z, w);
            Graphics g = panel1.CreateGraphics();
            tri.Draw_shape(g);
        }
        /// <summary>
        /// reset button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button1_Click(object sender, EventArgs e)
        {
            reset();
            MessageBox.Show("Reset made.");
        }
        /// <summary>
        /// clear button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button3_Click(object sender, EventArgs e)
        {
            clear();
        }

        /// <summary>
        /// Panel Paint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        /// <summary>
        /// save button while saving the button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text File (.txt)| *.txt";
            saveFileDialog.Title = "Save File...";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter fWriter = new StreamWriter(saveFileDialog.FileName);
                fWriter.Write(programbox.Text);
                fWriter.Close();
            }
            MessageBox.Show("Command saved successfully.");

        }
        /// <summary>
        /// load button to load the file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadFileDialog = new OpenFileDialog();
            loadFileDialog.Filter = "Text File (.txt)|*.txt";
            loadFileDialog.Title = "Open File...";

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamReader streamReader = 
                    new System.IO.StreamReader(loadFileDialog.FileName);
                programbox.Text = streamReader.ReadToEnd();
                streamReader.Close();
                MessageBox.Show("File loaded Successfully.");
            }
        }

       


    


        
       

        

       

        private void txtpen_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
           
            
        }

        private void commandbox_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Menu item for giving instructions on using program.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Instructions: \n" +
                "For Example: \n Programs: circle(70) || triangle(80, 50, 100) || " +
                "rectangle(50,50)  || moveto(50,50 ||drawto(50,50) || fill(on)" +
                "|| fill(off) || pen(blue) || pen(green) || pen(yellow) || pen(red) \n" +
                "Commands: run || clear || reset");
        }

        /// <summary>
        /// That displays about the program.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("About: \n This is a program designed to produce different shapes" +
                "\n"
                + "from the user input taken from textbox. For e.g: circle,"
                + "\n" + "triangle, rectangle and square. Moveto  moves the position and" +
                "reset changes position to initial. Command run executes all the program." +
                "Clear clears the drawing area. Drawto draws a line from the size given. Fill " +
                "on fills the shapes with choosed color and fill of doesnot make shape fill." +
                "There are 4 different pen colors and users can choose it from textbox." + "\n" +
                "Designed by: Joshna Poudel");
        }

        /// <summary>
        /// To exit the program.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Run button for running the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button2_Click(object sender, EventArgs e)
        {

            Graphics g = panel1.CreateGraphics();
            command = programbox.Text;
            multiLine = command.Split(')');


            for (int i = 0; i < multiLine.Length - 1; i++)
            {
                abc = multiLine[i].Trim();
                syntax = abc.Split('(');
                try
                {

                    if (commandbox.Text.Equals("run"))

                    {
                        if (syntax[0].Equals("moveto") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            pentomove(int.Parse(p1), int.Parse(p2));


                        }
                        else if (syntax[0].Equals("drawto") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            pentodraw(int.Parse(p1), int.Parse(p2));
                        }

                      



                        else if (syntax[0].Equals("rectangle") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            rectangle_draw(positionXaxis, positionYaxis, int.Parse(p1), int.Parse(p2));
                        }
                        else if (syntax[0].Equals("square") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter1[0];
                            square_draw(positionXaxis, positionYaxis, int.Parse(p1), int.Parse(p2));
                        }

                        else if (syntax[0].Equals("pen") == true)
                        {
                            this.penswitcher(syntax[1]);

                        }

                        else if (syntax[0].Equals("fill") == true)
                        {
                            if (syntax[1] == "on")
                            {
                                fill = true;
                            }
                            if ((syntax[1] == "off"))
                            {
                                fill = false;
                            }
                        }


                        else if (syntax[0].Equals("circle") == true)
                        {
                            String[] parameter2 = syntax[1].Split(')');
                            String p2 = parameter2[0];
                            circle_draw(positionXaxis, positionYaxis, int.Parse(p2));
                        }

                        else if (syntax[0].Equals("triangle") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(',');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            triangle_draw(positionXaxis, positionYaxis, int.Parse(p1), int.Parse(p2));
                        }

                        else
                        {
                            MessageBox.Show("The Syntax is invalid on line" + (i + 1));
                            panel1.Refresh();
                            break;

                        }

                    }


                } 
                catch (Exception)                //Exception thrown if Wrong parameter passed.
                {
                    MessageBox.Show("Wrong parameter passed.");


                }
            }





        }





        private void textBox2_KeyDown_1(object sender, KeyEventArgs e)
        {

          
            command = programbox.Text;
            multiLine = command.Split(')');


            for (int i = 0; i < multiLine.Length - 1; i++)
            {
                abc = multiLine[i].Trim();
                syntax = abc.Split('(');      //splitting by bracket
                try
                {

                    if (commandbox.Text.Equals("clear") == true && e.KeyCode == Keys.Enter)
                    {
                        clear();
                        MessageBox.Show("Drawing area cleared.");        //for clearing drawing area
                        
                    }

                    if (commandbox.Text.Equals("reset") == true && e.KeyCode == Keys.Enter)
                    {
                        reset();
                        MessageBox.Show("Reset to initial position made.");
                    }

                    if (commandbox.Text.Equals("run") == true && e.KeyCode == Keys.Enter)



                    {

                         


                        if (syntax[0].Equals("moveto") == true)    //for moving the position
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            pentomove(int.Parse(p1), int.Parse(p2));


                        }
                        else if (syntax[0].Equals("drawto") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            pentodraw(int.Parse(p1), int.Parse(p2));
                        }

                        

                        

                        else if (syntax[0].Equals("rectangle") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            rectangle_draw(positionXaxis, positionYaxis, int.Parse(p1), int.Parse(p2));
                        }
                        else if (syntax[0].Equals("square") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(')');
                            String p1 = parameter1[0];
                            String p2 = parameter1[0];
                            square_draw(positionXaxis, positionYaxis, int.Parse(p1), int.Parse(p2));
                        }

                        else if (syntax[0].Equals("pen") == true)
                        {
                            this.penswitcher(syntax[1]);

                        }

                        else if (syntax[0].Equals("fill") == true)     //if fill is true colors is given
                        {
                            if (syntax[1] == "on")
                            {
                                fill = true;
                            }
                            else
                            {
                                fill = false;
                            }
                        }


                        else if (syntax[0].Equals("circle") == true)
                        {
                            String[] parameter2 = syntax[1].Split(')');
                            String p2 = parameter2[0];
                            circle_draw(positionXaxis, positionYaxis, int.Parse(p2));
                        }

                        else if (syntax[0].Equals("triangle") == true)
                        {
                            String[] parameter1 = syntax[1].Split(',');
                            String[] parameter2 = parameter1[1].Split(',');
                            String p1 = parameter1[0];
                            String p2 = parameter2[0];
                            triangle_draw(positionXaxis, positionYaxis, int.Parse(p1), int.Parse(p2));
                        }

                        else if (commandbox.Text == "")
                        {
                            MessageBox.Show("Enter some value in the program!");
                        }

                        else
                        {
                            MessageBox.Show("The Syntax is invalid on line" + (i + 1));
                            panel1.Refresh();
                            break;

                        }

                    }


                }
                catch (Exception)
                {
                    MessageBox.Show("Wrong parameter passed.");   //exception thrown if wrong parameter passed.


                }
            }




        }

        private void commandbox_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
    