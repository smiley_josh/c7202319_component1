﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Component_I
{

    /// <summary>
    /// Class rectangle implemented from interface Shape
    /// </summary>
    public class Rectangle:Shape        //implementing the shape class
    {
        public int s, n, j, y;          //defining variables

        /// <summary>
        /// parameters of rectangles.
        /// </summary>
        /// <param name="a">one side</param>
        /// <param name="b">second side</param>
        /// <param name="c">third side</param>
        /// <param name="d">fourth side</param>
        public void saved_values(int a, int b, int c,int d)
        {
            s = a; 
            n = b;
            j = c;
            y = d;
        }

        /// <summary>
        /// Method for drawing rectangle
        /// </summary>
        /// <param name="g"></param>
        public void Draw_shape(Graphics g)
        {
          

            Pen pen = Form1.defaultpen;    //pen set to deafault. i.e. black   
            SolidBrush brush = Form1.sb;   //brush set to deafault. i.e. black   
            if (Form1.fill)
            {
                g.FillRectangle(brush, s, n, j, y);    //filled rectangle drawn
            }
            else
            {
                g.DrawRectangle(pen, s, n, j, y);      //not filled rectangle drawn
            }
        }
    }
}
