﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASE_Component_I
{
    /// <summary>
    /// Interface shape for inheritance usage.
    /// </summary>
   public interface Shape
    {
        /// <summary>
        /// Method for drawing different shapes according to user's choice.
        /// </summary>
        /// <param name="g">gives graphics to draw</param>
       void Draw_shape(Graphics g);     
    }
}
