﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Component_I
{
    /// <summary>
    /// Class square implemented from interface Shape
    /// </summary>
    class Square : Shape
    {

        public int s, n, j, y;              //defining variables of square
        public void saved_values(int a, int b, int c, int d)
        {
            s = a;                      //parameters of sqaure
            n = b;
            j = c;
            y = d;
        }

        /// <summary>
        /// Method for drawing square
        /// </summary>
        /// <param name="g"></param>
        public void Draw_shape(Graphics g)
        {
           

            Pen pen = Form1.defaultpen;
            SolidBrush brush = Form1.sb;
            if (Form1.fill)
            {
                g.FillRectangle(brush, s, n, j, y);    //filled square is produced
            }
            else
            {
                g.DrawRectangle(pen, s, n, j, y);
            }

        }

        
    }
}
