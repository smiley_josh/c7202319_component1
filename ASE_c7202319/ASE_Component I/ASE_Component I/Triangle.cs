﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Component_I
{
    /// <summary>
    /// Class Triangle implemented from interface Shape
    /// </summary>
    public class Triangle : Shape
    {
      /// <summary>
      /// three sides of triangle
      /// </summary>
        public int val1 , val2, bas, perp, hypt;

        
        /// <summary>
        /// values assigned to sides
        /// </summary>
        /// <param name="x">value1</param>
        /// <param name="y">value2</param>
        /// <param name="z">base</param>
        /// <param name="w">perpendicular</param>
        public void saved_values(int x, int y, int z, int w)
        {
            val1 = x;
            val2 = y;
            bas = z;
            perp = w;
        }

        /// <summary>
        /// Method for drawing triangle
        /// </summary>
        /// <param name="g"></param>
        public void Draw_shape(Graphics g)
        {
            
            PointF A = new Point(val1, val2);
            PointF B = new PointF(val1 + perp, val2);
            PointF C = new PointF(B.X, B.Y + perp);
            PointF[] points = { A, B, C };
           


            Pen pen = Form1.defaultpen;
            SolidBrush brush = Form1.sb;
            if (Form1.fill)
            {
                g.FillPolygon(brush, points);          //filled triangle is produced.
            }
            else
            {
                g.DrawPolygon(pen, points);
            }


        }
    }
}